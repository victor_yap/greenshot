@echo off
rem ###############################################################
rem  Convenient batch script for calling the crowdin-cli.
rem  crowdin-cli needs Java installed. 
rem  For more information, see http://crowdin.net/page/cli-tool
rem ###############################################################
rem  Greenshot - a free and open source screenshot tool
rem  Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
rem  
rem  For more information see: http://getgreenshot.org/
rem  The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
rem  
rem  This program is free software: you can redistribute it and/or modify
rem  it under the terms of the GNU General Public License as published by
rem  the Free Software Foundation, either version 1 of the License, or
rem  (at your option) any later version.
rem  
rem  This program is distributed in the hope that it will be useful,
rem  but WITHOUT ANY WARRANTY; without even the implied warranty of
rem  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
rem  GNU General Public License for more details.
rem  
rem  You should have received a copy of the GNU General Public License
rem  along with this program.  If not, see <http://www.gnu.org/licenses/>.
rem ###############################################################

if "%1" == "upload" (
	java -jar Greenshot\tools\crowdin\crowdin-cli.jar upload sources
) else if  "%1" == "upload-translations" (
	java -jar Greenshot\tools\crowdin\crowdin-cli.jar upload translations
) else if  "%1" == "download" (
	java -jar Greenshot\tools\crowdin\crowdin-cli.jar download
) else (
	echo Missing parameter.
	echo  Usage:
	echo  crowdin upload
	echo   - uploads ^(i.e. adds new or updates existing^) translation source files to crowdin
	echo  crowdin upload-translations
	echo   - uploads ^(i.e. adds new or updates existing^) translated files to crowdin
	echo  crowdin download
	echo   - downloads translated files from crowdin
	echo NOTE: you cannot upload files for a new language prior to setting up the language in the crowdin admin interface
)