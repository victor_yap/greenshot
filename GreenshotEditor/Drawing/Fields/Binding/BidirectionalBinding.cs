﻿using GreenshotPlugin.Drawing;
/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace GreenshotEditor.Drawing.Fields.Binding {
	/// <summary>
	/// Bidirectional binding of properties of two INotifyPropertyChanged instances.
	/// This implementation synchronizes null values, too. If you do not want this
	/// behavior (e.g. when binding to a 
	/// </summary>
	public class BidirectionalBinding {
		private INotifyPropertyChanged controlObject;
		private Surface surface;
		private string controlPropertyName;
		private FieldTypes fieldType;
		private bool updatingControl = false;
		private IBindingConverter converter;
		private IBindingValidator validator;
		private PropertyInfo controlValuePropertyInfo;
		private PropertyInfo controlVisiblePropertyInfo;
		
		/// <summary>
		/// Whether or not null values are passed on to the other object.
		/// </summary>
		protected bool AllowSynchronizeNull = true;
		
		/// <summary>
		/// Bind properties of two objects bidirectionally
		/// </summary>
		/// <param name="object1">Object containing 1st property to bind</param>
		/// <param name="property1">Property of 1st object to bind</param>
		/// <param name="object2">Object containing 2nd property to bind</param>
		/// <param name="property2">Property of 2nd object to bind</param>
		public BidirectionalBinding(INotifyPropertyChanged controlObject, string controlPropertyName, Surface surface, FieldTypes fieldType) {
			this.controlObject = controlObject;
			this.controlPropertyName = controlPropertyName;
			this.controlValuePropertyInfo = resolvePropertyInfo(controlObject, controlPropertyName);
			this.controlVisiblePropertyInfo = resolvePropertyInfo(controlObject, "Visible");
			this.surface = surface;
			this.fieldType = fieldType;
			
			this.controlObject.PropertyChanged += new PropertyChangedEventHandler(ControlPropertyChanged);
		}
		
		/// <summary>
		/// Bind properties of two objects bidirectionally, converting the values using a converter
		/// </summary>
		/// <param name="controlObject">Object containing 1st property to bind</param>
		/// <param name="controlPropertyName">Property of 1st object to bind</param>
		/// <param name="fieldObject">Object containing 2nd property to bind</param>
		/// <param name="fieldPropertyName">Property of 2nd object to bind</param>
		/// <param name="converter">taking care of converting the synchronzied value to the correct target format and back</param>
		public BidirectionalBinding(INotifyPropertyChanged controlObject, string controlPropertyName, Surface surface, FieldTypes fieldType, IBindingConverter converter)
			: this(controlObject, controlPropertyName, surface, fieldType) {
			this.converter = converter;
		}
		
		/// <summary>
		/// Bind properties of two objects bidirectionally, converting the values using a converter.
		/// Synchronization can be intercepted by adding a validator.
		/// </summary>
		/// <param name="controlObject">Object containing 1st property to bind</param>
		/// <param name="controlPropertyName">Property of 1st object to bind</param>
		/// <param name="fieldObject">Object containing 2nd property to bind</param>
		/// <param name="fieldPropertyName">Property of 2nd object to bind</param>
		/// <param name="validator">validator to intercept synchronisation if the value does not match certain criteria</param>
		public BidirectionalBinding(INotifyPropertyChanged controlObject, string controlPropertyName, Surface surface, FieldTypes fieldType, IBindingValidator validator)
			: this(controlObject, controlPropertyName, surface, fieldType) {
			this.validator = validator;
		}
		
		/// <summary>
		/// Bind properties of two objects bidirectionally, converting the values using a converter.
		/// Synchronization can be intercepted by adding a validator.
		/// </summary>
		/// <param name="controlObject">Object containing 1st property to bind</param>
		/// <param name="controlPropertyName">Property of 1st object to bind</param>
		/// <param name="fieldObject">Object containing 2nd property to bind</param>
		/// <param name="fieldPropertyName">Property of 2nd object to bind</param>
		/// <param name="converter">taking care of converting the synchronzied value to the correct target format and back</param>
		/// <param name="validator">validator to intercept synchronisation if the value does not match certain criteria</param>
		public BidirectionalBinding(INotifyPropertyChanged controlObject, string controlPropertyName, Surface surface, FieldTypes fieldType, IBindingConverter converter, IBindingValidator validator)
			: this(controlObject, controlPropertyName, surface, fieldType, converter) {
			this.validator = validator;
		}
		
		public void ControlPropertyChanged(object sender, PropertyChangedEventArgs e) {
			if (!updatingControl && e.PropertyName.Equals(controlPropertyName)) {
				updateFields();
			}
		}

		public void Refresh() {
			// Set control visibilty
			bool visible = surface.IsElementWithFieldTypeSelected(fieldType);
			controlVisiblePropertyInfo.SetValue(controlObject, visible, null);

			// No need to update when the control is not visible
			if (visible) {
				updatingControl = true;
				foreach (IFieldHolder fieldHolder in surface.ReturnSelectedElementsWithFieldType(fieldType)) {
					FieldAttribute attribute = fieldHolder.FieldAttributes[fieldType];
					if (attribute != null) {
						object currentValue = attribute.GetValue(fieldHolder);
						if (converter != null && currentValue != null) {
							currentValue = converter.convert(currentValue);
						}
						controlValuePropertyInfo.SetValue(controlObject, currentValue, null);
					}
				}
				updatingControl = false;
			}
		}

		private void updateFields() {
			object bValue = controlValuePropertyInfo.GetValue(controlObject, null);
			if (converter != null && bValue != null) {
				bValue = converter.convert(bValue);
			}
			surface.ChangeFields(fieldType, bValue);
		}
		
		private PropertyInfo resolvePropertyInfo(object obj, string property) {
			return obj.GetType().GetProperty(property);
		}
		
		public IBindingConverter Converter {
			get { return converter; }
			set { converter = value; }
		}
		
	}	
	
}
