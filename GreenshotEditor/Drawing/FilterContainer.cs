﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Drawing;
using GreenshotEditor.Drawing.Fields;
using GreenshotPlugin.Drawing;
using System.Drawing.Drawing2D;
using GreenshotEditor.Drawing.Filters;

namespace GreenshotEditor.Drawing {
	/// <summary>
	/// empty container for filter-only elements
	/// </summary>
	[Serializable()] 
	public abstract class FilterContainer : DrawableContainer {
		public enum PreparedFilter {BLUR, PIXELIZE, TEXT_HIGHTLIGHT, AREA_HIGHLIGHT, GRAYSCALE, MAGNIFICATION};

		protected int lineThickness = 0;
		[Field(FieldTypes.LINE_THICKNESS)]
		public int LineThickness {
			get {
				return lineThickness;
			}
			set {
				lineThickness = value;
				OnFieldPropertyChanged(FieldTypes.LINE_THICKNESS);
			}
		}

		protected Color lineColor = Color.Red;
		[Field(FieldTypes.LINE_COLOR)]
		public Color LineColor {
			get {
				return lineColor;
			}
			set {
				lineColor = value;
				OnFieldPropertyChanged(FieldTypes.LINE_COLOR);
			}
		}

		protected bool shadow = false;
		[Field(FieldTypes.SHADOW)]
		public bool Shadow {
			get {
				return shadow;
			}
			set {
				shadow = value;
				OnFieldPropertyChanged(FieldTypes.SHADOW);
			}
		}

		public abstract PreparedFilter Filter {
			get;
			set;
		}

		public FilterContainer(Surface parent) : base(parent) {
		}

		protected void ConfigurePreparedFilters() {
			Filters.Clear();
			switch (Filter) {
				case PreparedFilter.BLUR:
					Add(new BlurFilter(this));
					break;
				case PreparedFilter.PIXELIZE:
					Add(new PixelizationFilter(this));
					break;
				case PreparedFilter.TEXT_HIGHTLIGHT:
					Add(new HighlightFilter(this));
					break;
				case PreparedFilter.AREA_HIGHLIGHT:
					AbstractFilter bf = new BrightnessFilter(this);
					bf.Invert = true;
					Add(bf);
					bf = new BlurFilter(this);
					bf.Invert = true;
					Add(bf);
					break;
				case PreparedFilter.GRAYSCALE:
					AbstractFilter f = new GrayscaleFilter(this);
					f.Invert = true;
					Add(f);
					break;
				case PreparedFilter.MAGNIFICATION:
					Add(new MagnifierFilter(this));
					break;
			}
		}

		public override void Draw(Graphics graphics, RenderMode rm) {
			bool lineVisible = (lineThickness > 0 && Colors.IsVisible(lineColor));
			GraphicsState state = graphics.Save();
			if (lineVisible) {
				graphics.SmoothingMode = SmoothingMode.HighSpeed;
				graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
				graphics.CompositingQuality = CompositingQuality.HighQuality;
				graphics.PixelOffsetMode = PixelOffsetMode.None;
				//draw shadow first
				if (shadow) {
					int basealpha = 100;
					int alpha = basealpha;
					int steps = 5;
					int currentStep = lineVisible ? 1 : 0;
					while (currentStep <= steps) {
						using (Pen shadowPen = new Pen(Color.FromArgb(alpha, 100, 100, 100), lineThickness)) {
							Rectangle shadowRect = GuiRectangle.GetGuiRectangle(this.Left + currentStep, this.Top + currentStep, this.Width, this.Height);
							graphics.DrawRectangle(shadowPen, shadowRect);
							currentStep++;
							alpha = alpha - (basealpha / steps);
						}
					}
				}
				Rectangle rect = GuiRectangle.GetGuiRectangle(this.Left, this.Top, this.Width, this.Height);
				if (lineThickness > 0) {
					using (Pen pen = new Pen(lineColor, lineThickness)) {
						graphics.DrawRectangle(pen, rect);
					}
				}
				graphics.Restore(state);
			}
		}
	}
}
