﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text.RegularExpressions;
using System.Xml;
using GreenshotPlugin.Core;
using System.Threading.Tasks;
using System.Net.Http;
using System.IO;

namespace Greenshot.Helpers {
	public class InstallerFile {
		public string Filename {
			get;
			set;
		}
		public bool IsReleaseCandidate {
			get;
			set;
		}
		public bool IsUnstable {
			get;
			set;
		}
		public Version Version {
			get;
			set;
		}
	}

	public class BlogEntry {
		public string Title {
			get;
			set;
		}
		public DateTimeOffset Published {
			get;
			set;
		}
		public string Description {
			get;
			set;
		}
		public Uri Link {
			get;
			set;
		}
	}

	/// <summary>
	/// the UpdateChecker takes care of checking for updates.
	/// </summary>
	public class UpdateChecker {
		private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(UpdateChecker));
		private const string Updatefeed = "http://getgreenshot.org/project-feed";
		private const string Blogfeed = "http://getgreenshot.org/feed/";
		// The version to check against, this is update when newer files are found so we only report an update once!
		private readonly Version _currentVersion;

		/// <summary>
		/// Create the update checker with the current version
		/// </summary>
		/// <param name="currentVersion"></param>
		public UpdateChecker(Version currentVersion) {
			_currentVersion = currentVersion;
			BlogEntries = new SortedList<DateTimeOffset, BlogEntry>();
		}

		/// <summary>
		/// Sinmle helper method to get a version from a filename
		/// </summary>
		/// <param name="filename"></param>
		/// <returns></returns>
		private static string GetVersion(string filename) {
			var match = Regex.Match(filename, @"(\d{1,4}\.){3}\d{1,4}");
			return match.Success ? match.Groups[0].Value : null;
		}

		/// <summary>
		/// Current blog entries
		/// </summary>
		public SortedList<DateTimeOffset, BlogEntry> BlogEntries {
			get;
			set;
		}

		/// <summary>
		/// When we have a newer file as "this", the information on this file is stored here.
		/// </summary>
		public InstallerFile LatestInstaller {
			get;
			set;
		}

		/// <summary>
		/// Check if the blog has an update
		/// </summary>
		/// <returns>bool (async) when there is an update</returns>
		public async Task<bool> CheckBlogFeed() {
			HttpResponseMessage response = await NetworkHelper.HttpClient.GetAsync(Blogfeed);
			// Check that response was successful or throw exception
			response.EnsureSuccessStatusCode();

			var feedStream = await response.Content.ReadAsStreamAsync();
			var feed = SyndicationFeed.Load<SyndicationFeed>(new XmlTextReader(feedStream));
			if (feed == null) {
				return false;
			}
			bool hasUpdate = false;
			var blogEntries = from item in feed.Items
				where !BlogEntries.ContainsKey(item.PublishDate)
				select
					new BlogEntry {
						Title = item.Title.Text,
						Description = item.Summary.Text,
						Link = item.Links[0].GetAbsoluteUri(),
						Published = item.PublishDate
					};
			foreach (var blogEntry in blogEntries) {
				BlogEntries.Add(blogEntry.Published, blogEntry);
				hasUpdate = true;
			}
			return hasUpdate;
		}

		/// <summary>
		/// Check for program updates, return true if there are any
		/// </summary>
		/// <param name="checkRc"></param>
		/// <param name="checkUnstable"></param>
		/// <returns>bool (async)</returns>
		public async Task<bool> CheckForProgramUpdates(bool checkRc, bool checkUnstable) {
			HttpResponseMessage response = await NetworkHelper.HttpClient.GetAsync(Updatefeed);
			// Check that response was successful or throw exception
			response.EnsureSuccessStatusCode();

			bool hasUpdate = false;

			Stream feedStream = await response.Content.ReadAsStreamAsync();
			var feed = SyndicationFeed.Load<SyndicationFeed>(new XmlTextReader(feedStream));
			if (feed == null) {
				return false;
			}
			var installerFiles = from item in feed.Items
				where item.Title.Text.EndsWith(".exe") && item.Title.Text.Contains("-INSTALLER-")
				let version = GetVersion(item.Title.Text)
				where version != null
				select
					new InstallerFile {
						Filename = Regex.Replace(item.Title.Text, @".*/", ""),
						IsReleaseCandidate = item.Title.Text.ToLower().Contains("-rc"),
						IsUnstable = item.Title.Text.ToLower().Contains("unstable"),
						Version = new Version(version)
					};
			foreach (var installerFile in installerFiles) {
				// if the file is unstable, we will skip it when:
				// the current version is a release or release candidate AND check unstable is turned off.
				if (installerFile.IsUnstable && !checkUnstable) {
					// Skip if we shouldn't check unstables
					Log.DebugFormat("Ignoring as we don't check unstable: {0}", installerFile.Filename);
					continue;
				}

				// if the file is a release candidate, we will skip it when:
				// the current version is a release AND check unstable is turned off.
				if (installerFile.IsReleaseCandidate && !checkRc) {
					Log.DebugFormat("Ignoring as we don't check for rc: {0}", installerFile.Filename);
					continue;
				}

				// Compare versions
				if (installerFile.Version.CompareTo(_currentVersion) > 0) {
					// if current file newer than skip
					if (LatestInstaller != null && installerFile.Version.CompareTo(LatestInstaller.Version) <= 0) {
						Log.DebugFormat("Ignoring as we already have this: {0}", installerFile.Filename);
						continue;
					}
					LatestInstaller = installerFile;
					hasUpdate = true;
				} else {
					Log.DebugFormat("Ignoring as the version is lower: {0}", installerFile.Filename);
				}
			}
			return hasUpdate;
		}
	}
}
