################################################################
# Greenshot BUILD script, written for the Windows Power Shell
# Assumes the installation of Microsoft .NET Framework 4.5
################################################################
# Greenshot - a free and open source screenshot tool
# Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
# 
# For more information see: http://getgreenshot.org/
# The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 1 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
################################################################

Add-Type -Assembly System.ServiceModel.Web,System.Runtime.Serialization

# Collect GIT information
$gitversion = git describe --long
$gittag = $gitversion -replace '-.*',''
$commitversion = $gitversion -replace ($gittag + '-'),'' -replace '-.*',''
$githash = $gitversion -replace '.*-',''
$version = $gittag + '.' + $commitversion
$detailversion = $version + '-' + $githash
$readableversion = $gittag + ' build ' + $commitversion + " (" + $githash + ")"

# $languageFileIncludes = @('*.xml')
$languageFileIncludes = @('*.ini')

Function WaitForKey {
	$x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
	return
}

# Create a MD5 string for the supplied filename
Function MD5($filename) {
	$fileStream = new-object -TypeName System.IO.FileStream -ArgumentList "$filename", "Open", "Read", "Read"
	$MD5CryptoServiceProvider = new-object -TypeName System.Security.Cryptography.MD5CryptoServiceProvider
	$hash = $MD5CryptoServiceProvider.ComputeHash($fileStream)
	return [System.BitConverter]::ToString($hash) -replace "-", ""
}

# Convert a JSON string to XML, this is a workaround for not having a JSON parser
Function Convert-JsonToXml([string]$json) {
	$bytes = [byte[]][char[]]$json
	$quotas = [System.Xml.XmlDictionaryReaderQuotas]::Max
	$jsonReader = [System.Runtime.Serialization.Json.JsonReaderWriterFactory]::CreateJsonReader($bytes,$quotas)
	try {
		$xml = new-object System.Xml.XmlDocument
		$xml.Load($jsonReader)
	$xml
	} finally {
		$jsonReader.Close()
	}
}

# Create a WebClient
Function CreateWebClient {
	$proxy = [System.Net.WebRequest]::GetSystemWebProxy()
	$proxy.Credentials = [System.Net.CredentialCache]::DefaultCredentials

	$wc = new-object system.net.WebClient
	$wc.proxy = $proxy
	return $wc
}

# Create releasenotes for the commits
Function ReleaseNotes {
	# Get the git-log from tag to now
	$gitlog = (git shortlog "$gittag..HEAD")

	# The webclient can be used to get information from the internet, like a JIRA description.
	$wc = CreateWebClient

	# Array to store the descriptions
	$releaseNotes = @()

	# Find the jira's that are in the commits and place them in the release notes
	$jiras = $gitlog | foreach { [regex]::match($_,'[a-zA-Z0-9]+-[0-9]{1,5}').value -replace " ",""} | Where {$_ -match '\S'} | sort-object| select-object $_ -unique
	if ($null -ne $jiras) {
		$jiras | foreach {
			$jira = $_
			
			Write-Host "Checking JIRA for [$jira]"
			try {
				$jiraJson = $wc.DownloadString("https://greenshot.atlassian.net/rest/api/2/issue/$jira")
				$xml = Convert-JsonToXml $jiraJson
				$summary = $xml.root.fields.summary."#text"
				$releaseNotes += "* Bug $jira : $summary"
			} catch {
				Write-Host "Error reading JIRA [$jira] : $_"
			}
		}
	}

	# Find the sourceforge bugs's that are in the commits and place them in the release notes
	$bugs = $gitlog | foreach { [regex]::match($_,'\#[0-9]+').value -replace "\#","" -replace "\^","" -replace " ",""} | Where {$_ -match '\S'} | sort-object | select-object $_ -unique
	if ($null -ne $bugs) {
		$bugs | foreach {
			$bug = $_
			if ($bug -eq "") {
				continue
			}
			Write-Host "Checking Sourceforge for [#$bug]"
			try {
				$bugJson = $wc.DownloadString("https://sourceforge.net/rest/p/greenshot/bugs/$bug")
				$xml = Convert-JsonToXml $bugJson
				$summary = $xml.root.ticket.summary."#text"
				$releaseNotes +="* Bug #$bug : $summary"
			} catch {
				Write-Host "Error reading Bug [#$bug] : $_"
			}
		}
	}
	return $releaseNotes
}

# Fill the templates
Function FillTemplates {
	Write-Host "Filling templates for Git version $readableversion`n`n"
	
	$releaseNotes = ReleaseNotes
	Get-ChildItem . -recurse *.template | 
		foreach {
			$oldfile = $_.FullName
			$newfile = $_.FullName -replace '\.template',''
			Write-Host "Modifying file : $oldfile to $newfile"
			# Read the file
			$template = Get-Content $oldfile
			# Create an empty array, this will contain the replaced lines
			$newtext = @()
			foreach ($line in $template) {
				# Special case, if we find "@RELEASENOTES@" we replace that line with the release notes
				if ($line -match "\@RELEASENOTES\@") {
					$newtext += $releaseNotes
				} else {
					$newtext += $line -replace "\@GITVERSION\@", $version -replace "\@GITDETAILVERSION\@", $detailversion -replace "\@READABLEVERSION\@", $readableversion
				}
			}
			# Write the new information to the file
			$newtext | Set-Content $newfile -encoding UTF8
		}
}

# This function calls the Greenshot build
Function Build {
	$msBuild = "C:\Windows\Microsoft.NET\Framework64\v4.0.30319\MSBuild"
	if (-not (Test-Path("$msBuild"))) {
		$msBuild = "C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild"
	}
	$parameters = @('Greenshot\Greenshot.sln', '/t:Clean;Build', '/p:Configuration="Release"', '/p:Platform="Any CPU"')
	$buildOutput = "$(get-location)\build.log"
	Write-Host "Calling: $msBuild $parameters"
	$buildResult = Start-Process -wait -PassThru "$msBuild" -ArgumentList $parameters -NoNewWindow -RedirectStandardOutput $buildOutput
	if ($buildResult.ExitCode -ne 0) {
		Write-Host "An error occured, please check $BuildOutput for errors!"
		exit -1
	}
	return
}

# Create the MD5 checksum file
Function MD5Checksums {
	echo "MD5 Checksums:"
	$currentMD5 = MD5("$(get-location)\Greenshot\bin\Release\Greenshot.exe")
	echo "Greenshot.exe : $currentMD5"
	$currentMD5 = MD5("$(get-location)\Greenshot\bin\Release\GreenshotPlugin.dll")
	echo "GreenshotPlugin.dll : $currentMD5"
}

# Function to copy recurse with including & excluding
Function CopyRecurse([string]$source, [string]$dest, [string[]]$include, [string[]]$exclude = @()) {
	New-Item $dest -itemtype directory -EA silentlycontinue | Out-Null
	Get-ChildItem $source -Recurse -Exclude $exclude -Include $include | foreach {
		if ($_.Directory.Fullname -ne $source) {
			$destDir = Join-Path $dest $_.Directory.Name
			if (!(Test-Path $destDir)) {
				New-Item $destDir -itemtype directory -EA silentlycontinue | Out-Null
			}
		}
		$sourcefile = $_.Fullname
		$destfile = Join-Path $dest $sourcefile.Substring($source.length)
		Copy-Item $_.Fullname -Destination $destfile -Force
	}
}

# This function creates the paf.exe
Function PackagePortable {
	$sourcebase = "$(get-location)\Greenshot\bin\Release"
	$destbase = "$(get-location)\Greenshot\releases"

	# Only remove the paf we are going to create, to prevent adding but keeping the history
	if (Test-Path  ("$destbase\GreenshotPortable-$version.paf.exe")) {
		Remove-Item "$destbase\GreenshotPortable-$version.paf.exe" -Confirm:$false
	}
	# Remove the directory to create the files in
	if (Test-Path  ("$destbase\portabletmp")) {
		Remove-Item "$destbase\portabletmp" -recurse -Confirm:$false
	}
	Copy-Item -Path "$destbase\portable" -Destination "$destbase\portabletmp" -Recurse

	CopyRecurse "$sourcebase\Plugins\" "$destbase\portabletmp\App\Greenshot\Plugins\" @("*.gsp", "*.dll", "*.exe", "*.config")
	CopyRecurse "$(get-location)\Greenshot\Languages\" "$destbase\portabletmp\App\Greenshot\Languages\" $languageFileIncludes @("*installer*","*website*")
	CopyRecurse "$sourcebase\Languages\Plugins\" "$destbase\portabletmp\App\Greenshot\Languages\Plugins\" $languageFileIncludes @("*installer*","*website*")

	@( "$sourcebase\checksum.MD5",
		"$sourcebase\Greenshot.exe.config",
		"$sourcebase\GreenshotPlugin.dll",
		"$sourcebase\GreenshotEditor.dll",
		"$destbase\additional_files\*.txt" ) | foreach { Copy-Item $_ "$destbase\portabletmp\App\Greenshot\" }

	Copy-Item -Path "$sourcebase\Languages\help-en-US.html" -Destination "$destbase\portabletmp\help.html"

	Copy-Item -Path "$sourcebase\Greenshot.exe" -Destination "$destbase\portabletmp"

	Copy-Item -Path "$destbase\appinfo.ini" -Destination "$destbase\portabletmp\App\AppInfo\appinfo.ini"
		
	$portableOutput = "$(get-location)\portable"
	$portableInstaller = "$(get-location)\greenshot\tools\PortableApps.comInstaller\PortableApps.comInstaller.exe"
	$arguments = @("$destbase\portabletmp")
	Write-Host "Starting $portableInstaller $arguments"
	$portableResult = Start-Process -wait -PassThru "$portableInstaller" -ArgumentList $arguments -NoNewWindow -RedirectStandardOutput "$portableOutput.log" -RedirectStandardError "$portableOutput.error"
	if ($portableResult.ExitCode -ne 0) {
		Write-Host "An error occured, please check $portableOutput.log and $portableOutput.error for errors!"
		exit -1
	}
	Start-Sleep -m 1500
	Remove-Item "$destbase\portabletmp" -Recurse -Confirm:$false
	return
}

# This function creates the .zip
Function PackageZip {
	$sourcebase = "$(get-location)\Greenshot\bin\Release"
	$destbase = "$(get-location)\Greenshot\releases"
	$destinstaller = "$destbase\NO-INSTALLER"

	# Only remove the zip we are going to create, to prevent adding but keeping the history
	if (Test-Path  ("$destbase\Greenshot-NO-INSTALLER-$version.zip")) {
		Remove-Item "$destbase\Greenshot-NO-INSTALLER-$version.zip" -Confirm:$false
	}
	# Remove the directory to create the files in
	if (Test-Path  ("$destinstaller")) {
		Remove-Item "$destinstaller" -recurse -Confirm:$false
	}
	New-Item -ItemType directory -Path "$destinstaller" | Out-Null

	echo ";dummy config, used to make greenshot store the configuration in this directory" | Set-Content "$destinstaller\greenshot.ini" -encoding UTF8
	echo ";In this file you should add your default settings" | Set-Content "$destinstaller\greenshot-defaults.ini" -encoding UTF8
	echo ";In this file you should add your fixed settings" | Set-Content "$destinstaller\greenshot-fixed.ini" -encoding UTF8

	CopyRecurse "$sourcebase\Plugins\" "$destinstaller\Plugins\" @("*.gsp", "*.dll", "*.exe", "*.config")
	CopyRecurse "$(get-location)\Greenshot\Languages\" "$destinstaller\Languages\" $languageFileIncludes @("*installer*","*website*")
	CopyRecurse "$sourcebase\Languages\Plugins\" "$destinstaller\Languages\Plugins\" $languageFileIncludes @("*installer*","*website*")

	@( "$sourcebase\checksum.MD5",
		"$sourcebase\Greenshot.exe",
		"$sourcebase\Greenshot.exe.config",
		"$sourcebase\GreenshotPlugin.dll",
		"$sourcebase\GreenshotEditor.dll",
		"$destbase\additional_files\*.txt" ) | foreach { Copy-Item $_ "$destinstaller\" }

	$zipOutput = "$(get-location)\zip"
	$zip7 = "$(get-location)\greenshot\tools\7zip\7za.exe"
	$arguments = @('a', '-mx9', '-tzip', '-r', "$destbase\Greenshot-NO-INSTALLER-$version.zip", "$destinstaller\*")
	Write-Host "Starting $zip7 $arguments"
	$zipResult = Start-Process -wait -PassThru "$zip7" -ArgumentList $arguments -NoNewWindow -RedirectStandardOutput "$zipOutput.log" -RedirectStandardError "$zipOutput.error"
	if ($zipResult.ExitCode -ne 0) {
		Write-Host "An error occured, please check $zipOutput.log and $zipOutput.error for errors!"
		exit -1
	}
	Start-Sleep -m 1500
	Remove-Item "$destinstaller" -Recurse -Confirm:$false
	return
}

# This function creates the installer
Function PackageInstaller {
	$setupOutput = "$(get-location)\setup"
	$innoSetup = "$(get-location)\greenshot\tools\innosetup\ISCC.exe"
	$innoSetupFile = "$(get-location)\greenshot\releases\innosetup\setup.iss"
	Write-Host "Starting $innoSetup $innoSetupFile"
	$setupResult = Start-Process -wait -PassThru "$innoSetup" -ArgumentList "$innoSetupFile" -NoNewWindow -RedirectStandardOutput "$setupOutput.log" -RedirectStandardError "$setupOutput.error"
	if ($setupResult.ExitCode -ne 0) {
		Write-Host "An error occured, please check $setupOutput.log and $setupOutput.error for errors!"
		exit -1
	}
	return
}

FillTemplates

$continue = Read-Host "`n`nPreperations are ready.`nIf you are generating a release you can now change the readme.txt to your desire and use 'y' afterwards to continue building.`nContinue with the build? (y/n)"

if ($continue -ne "y") {
	Write-Host "skipped build."
	exit 0
}
Build

echo "Generating MD5"

MD5Checksums | Set-Content "$(get-location)\Greenshot\bin\Release\checksum.MD5" -encoding UTF8

echo "Generating Installer"
PackageInstaller

echo "Generating ZIP"
PackageZip

echo "Generating Portable"
PackagePortable

echo "Ready, press any key to continue!"

WaitForKey