import com.feature50.util.StringUtils
import com.sun.org.apache.xerces.internal.jaxp.SAXParserImpl
import org.xml.sax.XMLReader

import javax.xml.parsers.SAXParser

/**
 * Converts language XML to INI file. Will create new file with .ini extension, but not overwrite existing files.
 * If a directory is passed, all files matching language-*.xml will be processed
 *
 * Arguments:
 *  * path to XML file or directory
 *
 *  E.g.
 *  <code>groovy -c UTF8 convert "C:\path\to\Greenshot\Languages"</code>
 *  or
 *  <code>groovy -c UTF8 convert "C:\path\to\Greenshot\Languages\language-de-DE.xml"</code>
 */



def input = new File(args[0])
if(input.isDirectory()) {
    input.eachFileRecurse {convert(it)}
} else {
    convert(input)
}
def convert(def inputFile) {
    def filename = inputFile.name
    if(!filename.endsWith('.xml') || !filename.startsWith('language') || !inputFile.isFile()) {
        println "Not valid: $inputFile, skipping";
        return
    }

    def out = new StringBuilder()

    def lang = new XmlSlurper().parse(inputFile)
	out << "\n[language]"
	out << "\n;Name of this language, as written in itself, e.g. English, Deutsch, Nederlands, Pусский"
    out << "\ndescription=" << lang.@description
    out << "\n;IETF code of this language, e.g. en-US, de-DE, nl-NL, ru-RU"
	out << "\nietf=" << lang.@ietf
    out << "\n;Greenshot version this translation has been made for"
	out << "\nversion=" << lang.@version
	out << "\n;language group this language belongs to, see http://msdn.microsoft.com/en-us/goglobal/bb964663#EVD"
    out << "\nlanguagegroup=" << lang.@languagegroup
    if(!lang.@prefix.isEmpty()) out << "\nprefix=" << lang.@prefix

	out << "\n\n[messages]"
    lang.resources.resource.each {
        def key = it.@name
        def val = it.text().trim()
        val = val.replaceAll(/(?m)\r\n|\r|\n/,'\\\\n')
        //val = val.replaceAll("(:|!|=|#)",'\\\\$1')
        out << "\n$key=$val"
    }

    String propFilename = inputFile.canonicalPath.replace('.xml', '.ini')
    def f = new File(propFilename)
    if(f.exists()) {
       println "Won't overwrite existing file $propFilename, please delete or rename it first."
    } else {
        f.write(out.toString(), "UTF-8")
        println "Successfully wrote to file $propFilename"
    }
}

