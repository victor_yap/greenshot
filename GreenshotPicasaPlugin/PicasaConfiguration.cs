﻿/*
 * A Picasa Plugin for Greenshot
 * Copyright (C) 2011  Francis Noel
 * 
 * For more information see: http://getgreenshot.org/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Greenshot.IniFile;
using GreenshotPlugin.Core;
using System.Windows;

namespace GreenshotPicasaPlugin {
	/// <summary>
	/// Description of PicasaConfiguration.
	/// </summary>
	[IniSection("Picasa", Description = "Greenshot Picasa Plugin configuration")]
	public class PicasaConfiguration : IniSection {
		[IniProperty("UploadFormat", Description = "What file type to use for uploading", DefaultValue = "png")]
		public OutputFormat UploadFormat {
			get;
			set;
		}

		[IniProperty("UploadJpegQuality", Description="JPEG file save quality in %.", DefaultValue="80")]
		public int UploadJpegQuality {
			get;
			set;
		}

		[IniProperty("AfterUploadLinkToClipBoard", Description = "After upload send Picasa link to clipboard.", DefaultValue = "true")]
		public bool AfterUploadLinkToClipBoard {
			get;
			set;
		}

		[IniProperty("PicasaToken", Description = "Picasa Token", Encrypted = true, ExcludeIfNull=true)]
		public string PicasaToken {
			get;
			set;
		}

		[IniProperty("PicasaTokenSecret", Description = "PicasaTokenSecret", Encrypted = true, ExcludeIfNull = true)]
		public string PicasaTokenSecret {
			get;
			set;
		}

		/// <summary>
		/// Property is stored in Greenshot constants
		/// </summary>
		[IniProperty("ConsumerKey", ReadOnly = true, Encrypted = true, Description = "Picasa Consumer Key", ExcludeIfNull = true)]
		public string ConsumerKey {
			get;
			set;
		}

		/// <summary>
		/// Property is stored in Greenshot constants
		/// </summary>
		[IniProperty("ConsumerSecret", ReadOnly = true, Encrypted = true, Description = "Picasa Consumer Secret", ExcludeIfNull = true)]
		public string ConsumerSecret {
			get;
			set;
		}

		public override void AfterLoad() {
			base.AfterLoad();
			if (string.IsNullOrEmpty(ConsumerSecret) || string.IsNullOrEmpty(ConsumerKey)) {
				MessageBox.Show("Missing Picasa credentials, upload to Picasa won't work!", "Picasa Plugin", MessageBoxButton.OK, MessageBoxImage.Information);
			}
		}
	}
}
