﻿
using GreenshotPlugin.UnmanagedHelpers;
/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Media;
using GreenshotPlugin.Core.Imaging;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace GreenshotPlugin.Core.Capturing {
	/// <summary>
	/// This class holds all the available information on a Window.
	/// The information is updated (or reset) by the WinEventHook
	/// </summary>
	public class WindowInfo : INotifyPropertyChanged {
		private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(WindowInfo));
		public event PropertyChangedEventHandler PropertyChanged;

		#region USER32 DllImports
		[DllImport("user32", SetLastError = true)]
		private static extern IntPtr GetParent(IntPtr hWnd);
		[DllImport("user32", CharSet = CharSet.Unicode, SetLastError = true)]
		private extern static int GetWindowText(IntPtr hWnd, StringBuilder lpString, int cch);
		[DllImport("user32", CharSet = CharSet.Unicode, SetLastError = true)]
		private extern static int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);
		[DllImport("user32", SetLastError = true)]
		private extern static IntPtr SendMessage(IntPtr hWnd, uint wMsg, IntPtr wParam, IntPtr lParam);
		[DllImport("user32", SetLastError = true)]
		private static extern IntPtr GetClassLong(IntPtr hWnd, int nIndex);
		[DllImport("user32", SetLastError = true)]
		private static extern IntPtr GetClassLongPtr(IntPtr hWnd, int nIndex);

		/// <summary>
		/// Wrapper for the GetClassLong which decides if the system is 64-bit or not and calls the right one.
		/// </summary>
		/// <param name="hWnd">IntPtr</param>
		/// <param name="nIndex">int</param>
		/// <returns>IntPtr</returns>
		public static IntPtr GetClassLongWrapper(IntPtr hWnd, int nIndex) {
			if (IntPtr.Size > 4) {
				return GetClassLongPtr(hWnd, nIndex);
			} else {
				return GetClassLong(hWnd, nIndex);
			}
		}
		#endregion

		/// <summary>
		/// Retrieve the windows title, also called Text
		/// </summary>
		/// <param name="hWnd">IntPtr for the window</param>
		/// <returns>string</returns>
		private static string getText(IntPtr hWnd) {
			StringBuilder title = new StringBuilder(260, 260);
			int length = GetWindowText(hWnd, title, title.Capacity);
			if (length == 0) {
				Win32Error error = Win32.GetLastErrorCode();
				if (error != Win32Error.Success) {
					return null;
				}
			}
			return title.ToString();
		}
		/// <summary>
		/// Retrieve the windows classname
		/// </summary>
		/// <param name="hWnd">IntPtr for the window</param>
		/// <returns>string</returns>
		private static string getClassname(IntPtr hWnd) {
			StringBuilder classNameBuilder = new StringBuilder(260, 260);
			int hresult = GetClassName(hWnd, classNameBuilder, classNameBuilder.Capacity);
			if (hresult == 0) {
				return null;
			}
			return classNameBuilder.ToString();
		}


		/// <summary>
		/// Get the icon for a hWnd
		/// </summary>
		/// <param name="hWnd"></param>
		/// <returns></returns>
		private static ImageSource GetIcon(IntPtr hWnd) {
			IntPtr ICON_SMALL = IntPtr.Zero;
			IntPtr ICON_BIG = new IntPtr(1);
			IntPtr ICON_SMALL2 = new IntPtr(2);

			IntPtr iconHandle = SendMessage(hWnd, (int)WindowsMessages.WM_GETICON, ICON_SMALL2, IntPtr.Zero);
			if (iconHandle == IntPtr.Zero) {
				iconHandle = SendMessage(hWnd, (int)WindowsMessages.WM_GETICON, ICON_SMALL, IntPtr.Zero);
			}
			if (iconHandle == IntPtr.Zero) {
				iconHandle = GetClassLongWrapper(hWnd, (int)ClassLongIndex.GCL_HICONSM);
			}
			if (iconHandle == IntPtr.Zero) {
				iconHandle = SendMessage(hWnd, (int)WindowsMessages.WM_GETICON, ICON_BIG, IntPtr.Zero);
			}
			if (iconHandle == IntPtr.Zero) {
				iconHandle = GetClassLongWrapper(hWnd, (int)ClassLongIndex.GCL_HICON);
			}

			if (iconHandle == IntPtr.Zero) {
				return null;
			}

			using (System.Drawing.Icon icon = System.Drawing.Icon.FromHandle(iconHandle)) {
				return icon.ToImageSource();
			}
		}

		/// <summary>
		/// A helper method to create the WindowInfo for a hWnd with it's parent
		/// </summary>
		/// <param name="hWnd">IntPtr</param>
		/// <returns>WindowInfo</returns>
		public static WindowInfo CreateFor(IntPtr hWnd) {
			return new WindowInfo {
				Handle = hWnd,
				Classname = getClassname(hWnd),
				Parent = GetParent(hWnd)
			};
		}

		/// <summary>
		/// Equals override
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public override bool Equals(Object obj) {
			// Check for null values and compare run-time types.
			if (obj == null || GetType() != obj.GetType())
				return false;

			WindowInfo wi = (WindowInfo)obj;
			return wi.Handle == Handle;
		}

		/// <summary>
		/// GetHashCode override
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode() {
			return Handle.GetHashCode();
		}

		/// <summary>
		/// The hWnd
		/// </summary>
		public IntPtr Handle {
			get;
			set;
		}

		/// <summary>
		/// The parent hWnd
		/// </summary>
		public IntPtr Parent {
			get;
			set;
		}

		/// <summary>
		/// Returns true if this WindowInfo has a parent
		/// </summary>
		public bool HasParent {
			get {
				return IntPtr.Zero != Parent;
			}
		}

		private List<WindowInfo> children = new List<WindowInfo>();
		/// <summary>
		/// List of children
		/// </summary>
		public List<WindowInfo> Children {
			get {
				return children;
			}
		}


		private string text = null;
		/// <summary>
		/// Property for the "title" of the window, use "Property = null" to update
		/// </summary>
		public string Text {
			get {
				if (text == null) {
					text = getText(Handle);
				}
				return text;
			}
			set {
				text = value;
				this.PropertyChanged.Notify(() => this.Text);
			}
		}

		private string classname = null;
		/// <summary>
		/// Classname of this window
		/// </summary>
		public string Classname {
			get {
				if (classname == null) {
					classname = getClassname(Handle);
				}
				return this.classname;
			}
			set {
				classname = value;
				this.PropertyChanged.Notify(() => this.Classname);
			}
		}

		/// <summary>
		/// A simple check if this window has a classname, this skips any retrieval
		/// </summary>
		public bool HasClassname {
			get {
				return classname != null;
			}
		}

		private Rectangle bounds;
		/// <summary>
		/// Bounds for this window
		/// </summary>
		public Rectangle Bounds {
			get {
				return this.bounds;
			}
			set {
				if (value != Rectangle.Empty) {
					bounds = value;
				} else {
					Rectangle windowRect = Rectangle.Empty;
					if (!HasParent && DWM.isDWMEnabled()) {
						DWM.GetExtendedFrameBounds(Handle, out windowRect);
					}

					if (windowRect.IsEmpty) {
						User32.GetWindowRect(Handle, out windowRect);
					}

					// Correction for maximized windows, only if it's not an app
					if (!HasParent && !isApp && Maximised) {
						Size size = Size.Empty;
						User32.GetBorderSize(Handle, out size);
						windowRect = new Rectangle(windowRect.X + size.Width, windowRect.Y + size.Height, windowRect.Width - (2 * size.Width), windowRect.Height - (2 * size.Height));
					}
					bounds = windowRect;
				}
				this.PropertyChanged.Notify(() => this.Bounds);
			}
		}

		/// <summary>
		/// Checks if this window is maximized
		/// </summary>
		public bool Maximised {
			get {
				if (isApp) {
					if (Visible) {
						foreach (Screen screen in Screen.AllScreens) {
							if (bounds.Equals(screen.Bounds)) {
								return true;
							}
						}
					}
					return false;
				}
				return User32.IsZoomed(Handle);
			}
		}

		/// <summary>
		/// Gets whether the window is visible.
		/// </summary>
		public bool Visible {
			get {
				if (isApp) {
					return ModernUI.AppVisible(Bounds);
				}
				if (isGutter) {
					// gutter is only made available when it's visible
					return true;
				}
				if (isAppLauncher) {
					return ModernUI.IsLauncherVisible;
				}
				return User32.IsWindowVisible(Handle);
			}
		}

		private ImageSource displayIcon;
		/// <summary>
		/// Returns an imagesource with the icon for this window
		/// </summary>
		public ImageSource DisplayIcon {
			get {
				if (displayIcon == null) {
					displayIcon = GetIcon(Handle);
				}
				return this.displayIcon;
			}
			set {
				displayIcon = value;
			}
		}

		/// <summary>
		/// Returns true if this window is an App
		/// </summary>
		public bool isApp {
			get {
				return ModernUI.MODERNUI_WINDOWS_CLASS.Equals(classname);
			}
		}

		/// <summary>
		/// Returns true if this window is the app "gutter"
		/// </summary>
		public bool isGutter {
			get {
				return ModernUI.MODERNUI_GUTTER_CLASS.Equals(classname);
			}
		}

		/// <summary>
		/// Returns true if this window is the app launcher (Windows 8 start screen)
		/// </summary>
		public bool isAppLauncher {
			get {
				return ModernUI.MODERNUI_APPLAUNCHER_CLASS.Equals(classname);
			}
		}

		/// <summary>
		/// Check if this window is the window of a metro app
		/// </summary>
		public bool isMetroApp {
			get {
				return isAppLauncher || isApp;
			}
		}
	}
}
