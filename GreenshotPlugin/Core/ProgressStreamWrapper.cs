﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.IO;

namespace GreenshotPlugin.Core {
	public class ProgressStreamWrapper : Stream, IDisposable {
		private IProgress<long> progress;
		private long length;
		public ProgressStreamWrapper(Stream innerStream, IProgress<long> progress, long length) {
			this.length = length;
			InnerStream = innerStream;
			this.progress = progress;
		}

		public Stream InnerStream {
			get;
			private set;
		}

		public override void Close() {
			InnerStream.Close();
		}

		void IDisposable.Dispose() {
			base.Dispose();
			InnerStream.Dispose();
		}

		public override void Flush() {
			InnerStream.Flush();
		}

		public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback callback, object state) {
			return InnerStream.BeginRead(buffer, offset, count, callback, state);
		}

		public override int EndRead(IAsyncResult asyncResult) {
			int endRead = InnerStream.EndRead(asyncResult);
			OnPositionChanged();
			return endRead;
		}

		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback callback, object state) {
			return InnerStream.BeginWrite(buffer, offset, count, callback, state);
		}

		public override void EndWrite(IAsyncResult asyncResult) {
			InnerStream.EndWrite(asyncResult);
			OnPositionChanged();
		}

		public override long Seek(long offset, SeekOrigin origin) {
			long seek = InnerStream.Seek(offset, origin);
			OnPositionChanged();
			return seek;
		}

		public override void SetLength(long value) {
			InnerStream.SetLength(value);
		}

		public override int Read(byte[] buffer, int offset, int count) {
			int read = InnerStream.Read(buffer, offset, count);
			OnPositionChanged();
			return read;
		}

		public override int ReadByte() {
			int readByte = InnerStream.ReadByte();
			OnPositionChanged();
			return readByte;
		}

		public override void Write(byte[] buffer, int offset, int count) {
			InnerStream.Write(buffer, offset, count);
			OnPositionChanged();
		}

		public override void WriteByte(byte value) {
			InnerStream.WriteByte(value);
			OnPositionChanged();
		}

		public override bool CanRead {
			get {
				return InnerStream.CanRead;
			}
		}

		public override bool CanSeek {
			get {
				return InnerStream.CanSeek;
			}
		}

		public override bool CanTimeout {
			get {
				return InnerStream.CanTimeout;
			}
		}

		public override bool CanWrite {
			get {
				return InnerStream.CanWrite;
			}
		}

		public override long Length {
			get {
				return InnerStream.Length;
			}
		}

		public override long Position {
			get {
				return InnerStream.Position;
			}
			set {
				InnerStream.Position = value;
				OnPositionChanged();
			}
		}

		protected virtual void OnPositionChanged() {
			if (progress != null) {
				if (length > 0) {
					progress.Report((100 * Position) / length);
				} else {
					progress.Report(Position);
				}
			}
		}

		public override int ReadTimeout {
			get {
				return InnerStream.ReadTimeout;
			}
			set {
				InnerStream.ReadTimeout = value;
			}
		}

		public override int WriteTimeout {
			get {
				return InnerStream.WriteTimeout;
			}
			set {
				InnerStream.WriteTimeout = value;
			}
		}
	}
}
