﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Greenshot.IniFile;
using GreenshotPlugin;
using GreenshotPlugin.UnmanagedHelpers;
using GreenshotPlugin.Core;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using Screen = System.Windows.Forms.Screen;
using Region = System.Drawing.Region;
using System.Windows.Media;

namespace GreenshotPlugin.WPF.Capture {
	/// <summary>
	/// The screen capture code
	/// </summary>
	public static class ScreenCapture {
		private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(WindowCapture));
		private static CoreConfiguration conf = IniConfig.GetIniSection<CoreConfiguration>();

		/// <summary>
		/// Used to cleanup the unmanged resource in the iconInfo for the CaptureCursor method
		/// </summary>
		/// <param name="hObject"></param>
		/// <returns></returns>
		[DllImport("gdi32", SetLastError = true)]
		private static extern bool DeleteObject(IntPtr hObject);

		/// <summary>
		/// Get the bounds of all screens combined.
		/// </summary>
		/// <returns>A Rect of the bounds of the entire display area.</returns>
		public static Rect GetScreenBounds() {
			int left = 0, top = 0, bottom = 0, right = 0;
			foreach (Screen screen in Screen.AllScreens) {
				left = Math.Min(left, screen.Bounds.X);
				top = Math.Min(top, screen.Bounds.Y);
				int screenAbsRight = screen.Bounds.X + screen.Bounds.Width;
				int screenAbsBottom = screen.Bounds.Y + screen.Bounds.Height;
				right = Math.Max(right, screenAbsRight);
				bottom = Math.Max(bottom, screenAbsBottom);
			}
			return new Rect(left, top, (right + Math.Abs(left)), (bottom + Math.Abs(top)));
		}
		
		/// <summary>
		/// Retrieves the cursor location safely, accounting for DPI settings in Vista/Windows 7.
		/// <returns>Point with cursor location, relative to the origin of the monitor setup (i.e. negative coordinates are
		/// possible in multiscreen setups)</returns>
		public static Point GetCursorLocation() {
			POINT cursorLocation;
			if (User32.GetPhysicalCursorPos(out cursorLocation)) {
				return new Point(cursorLocation.X, cursorLocation.Y);
			} else {
				Win32Error error = Win32.GetLastErrorCode();
				LOG.ErrorFormat("Error retrieving PhysicalCursorPos : {0}", Win32.GetMessage(error));
			}
			return new Point(100, 100);
		}
		
		/// <summary>
		/// Retrieves the cursor location safely, accounting for DPI settings in Vista/Windows 7. This implementation
		/// can conveniently be used when the cursor location is needed to deal with a fullscreen bitmap.
		/// <returns>Point with cursor location, relative to the top left corner of the monitor setup (which itself might
		/// actually not be on any screen)</returns>
		public static Point GetCursorLocationRelativeToScreenBounds() {
			return GetLocationRelativeToScreenBounds(GetCursorLocation());
		}
		
		/// <summary>
		/// Converts locationRelativeToScreenOrigin to be relative to top left corner of all screen bounds, which might
		/// be different in multiscreen setups. This implementation
		/// can conveniently be used when the cursor location is needed to deal with a fullscreen bitmap.
		/// </summary>
		/// <param name="locationRelativeToScreenOrigin"></param>
		/// <returns>Point</returns>
		public static Point GetLocationRelativeToScreenBounds(Point locationRelativeToScreenOrigin) {
			Point ret = locationRelativeToScreenOrigin;
			Rect bounds = GetScreenBounds();
			ret.Offset(-bounds.X, -bounds.Y);
			return ret;
		}

		/// <summary>
		/// This method will capture the current Cursor by using User32 Code
		/// </summary>
		/// <returns>A IElement with the Mouse Cursor as Image in it.</returns>
		public static IElement<BitmapSource> CaptureCursor() {
			LOG.Debug("Capturing the mouse cursor.");
			IElement<BitmapSource> element = new BitmapSourceElement();

			CURSORINFO cursorInfo = new CURSORINFO(); 
			IconInfo iconInfo;
			cursorInfo.cbSize = Marshal.SizeOf(cursorInfo);
			if (User32.GetCursorInfo(out cursorInfo)) {
				if (cursorInfo.flags == User32.CURSOR_SHOWING) { 
					using (SafeIconHandle safeIcon = User32.CopyIcon(cursorInfo.hCursor)) {
						if (User32.GetIconInfo(safeIcon, out iconInfo)) {
							Point cursorLocation = GetCursorLocation();
							element.Content = Imaging.CreateBitmapSourceFromHIcon(safeIcon.DangerousGetHandle(), Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
							element.Bounds = new Rect(cursorLocation.X - iconInfo.xHotspot, cursorLocation.Y - iconInfo.yHotspot, element.Content.Width, element.Content.Height);

							if (iconInfo.hbmMask != IntPtr.Zero) {
								DeleteObject(iconInfo.hbmMask);
							}
							if (iconInfo.hbmColor != IntPtr.Zero) {
								DeleteObject(iconInfo.hbmColor);
							}
						}
					}
				}
			}
			return element;
		}

	
		/// <summary>
		/// Helper method to create an exception that might explain what is wrong while capturing
		/// </summary>
		/// <param name="method">string with current method</param>
		/// <param name="capture">ICapture</param>
		/// <param name="captureBounds">Rectangle of what we want to capture</param>
		/// <returns></returns>
		private static Exception CreateCaptureException(string method, Rect captureBounds) {
			Exception exceptionToThrow = User32.CreateWin32Exception(method);
			if (!captureBounds.IsEmpty) {
				exceptionToThrow.Data.Add("Height", captureBounds.Height);
				exceptionToThrow.Data.Add("Width", captureBounds.Width);
			}
			return exceptionToThrow;
		}

		/// <summary>
		/// This method will use User32 code to capture the specified captureBounds from the screen
		/// </summary>
		/// <param name="captureBounds">Rectangle with the bounds to capture</param>
		/// <returns>Bitmap which is captured from the screen at the location specified by the captureBounds</returns>
		public static IElement<BitmapSource> CaptureRectangle(Rect captureBounds) {
			IElement<BitmapSource> element = null;
			if (captureBounds.Height <= 0 || captureBounds.Width <= 0) {
				LOG.Warn("Nothing to capture, ignoring!");
				return element;
			} else {
				LOG.Debug("CaptureRectangle Called!");
			}
			// .NET GDI+ Solution, according to some post this has a GDI+ leak...
			// See http://connect.microsoft.com/VisualStudio/feedback/details/344752/gdi-object-leak-when-calling-graphics-copyfromscreen
			// Bitmap capturedBitmap = new Bitmap(captureBounds.Width, captureBounds.Height);
			// using (Graphics graphics = Graphics.FromImage(capturedBitmap)) {
			//	graphics.CopyFromScreen(captureBounds.Location, Point.Empty, captureBounds.Size, CopyPixelOperation.CaptureBlt);
			// }
			// capture.Image = capturedBitmap;
			// capture.Location = captureBounds.Location;

			using (SafeWindowDCHandle desktopDCHandle = SafeWindowDCHandle.fromDesktop()) {
				if (desktopDCHandle.IsInvalid) {
					// Get Exception before the error is lost
					Exception exceptionToThrow = CreateCaptureException("desktopDCHandle", captureBounds);
					// throw exception
					throw exceptionToThrow;
				}

				// create a device context we can copy to
				using (SafeCompatibleDCHandle safeCompatibleDCHandle = GDI32.CreateCompatibleDC(desktopDCHandle)) {
					// Check if the device context is there, if not throw an error with as much info as possible!
					if (safeCompatibleDCHandle.IsInvalid) {
						// Get Exception before the error is lost
						Exception exceptionToThrow = CreateCaptureException("CreateCompatibleDC", captureBounds);
						// throw exception
						throw exceptionToThrow;
					}
					// Create BitmapInfoHeader for CreateDIBSection
					BITMAPINFOHEADER bmi = new BITMAPINFOHEADER(captureBounds.Width, captureBounds.Height, 24);

					// Make sure the last error is set to 0
					Win32.SetLastError(0);

					// create a bitmap we can copy it to, using GetDeviceCaps to get the width/height
					IntPtr bits0; // not used for our purposes. It returns a pointer to the raw bits that make up the bitmap.
					using (SafeDibSectionHandle safeDibSectionHandle = GDI32.CreateDIBSection(desktopDCHandle, ref bmi, BITMAPINFOHEADER.DIB_RGB_COLORS, out bits0, IntPtr.Zero, 0)) {
						if (safeDibSectionHandle.IsInvalid) {
							// Get Exception before the error is lost
							Exception exceptionToThrow = CreateCaptureException("CreateDIBSection", captureBounds);
							exceptionToThrow.Data.Add("hdcDest", safeCompatibleDCHandle.DangerousGetHandle().ToInt32());
							exceptionToThrow.Data.Add("hdcSrc", desktopDCHandle.DangerousGetHandle().ToInt32());

							// Throw so people can report the problem
							throw exceptionToThrow;
						} else {
							// select the bitmap object and store the old handle
							using (SafeSelectObjectHandle selectObject = safeCompatibleDCHandle.SelectObject(safeDibSectionHandle)) {
								// bitblt over (make copy)
								GDI32.BitBlt(safeCompatibleDCHandle, 0, 0, (int)captureBounds.Width, (int)captureBounds.Height, desktopDCHandle, (int)captureBounds.X, (int)captureBounds.Y, System.Drawing.CopyPixelOperation.SourceCopy | System.Drawing.CopyPixelOperation.CaptureBlt);
							}

							// get a .NET image object for it
							// A suggestion for the "A generic error occurred in GDI+." E_FAIL/0×80004005 error is to re-try...
							bool success = false;
							ExternalException exception = null;
							for (int i = 0; i < 3; i++) {
								try {
									// Create BitmapSource from the DibSection
									element.Content = Imaging.CreateBitmapSourceFromHBitmap(safeDibSectionHandle.DangerousGetHandle(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());

									// Now cut away invisible parts

									// Collect all screens inside this capture
									List<Screen> screensInsideCapture = new List<Screen>();
									foreach (Screen screen in Screen.AllScreens) {
										if (screen.Bounds.IntersectsWith(captureBounds.toRectangle())) {
											screensInsideCapture.Add(screen);
										}
									}
									// Check all all screens are of an equal size
									bool offscreenContent = false;
									using (Region captureRegion = new Region(captureBounds.toRectangle())) {
										// Exclude every visible part
										foreach (Screen screen in screensInsideCapture) {
											captureRegion.Exclude(screen.Bounds);
										}
										// If the region is not empty, we have "offscreenContent"
										using (System.Drawing.Graphics screenGraphics = System.Drawing.Graphics.FromHwnd(User32.GetDesktopWindow())) {
											offscreenContent = !captureRegion.IsEmpty(screenGraphics);
										}
									}
									// Check if we need to have a transparent background, needed for offscreen content
									if (offscreenContent) {
										WriteableBitmap modifiedImage = new WriteableBitmap(element.Content.PixelWidth, element.Content.PixelHeight, element.Content.DpiX, element.Content.DpiY, PixelFormats.Bgr32, element.Content.Palette);
										foreach (Screen screen in Screen.AllScreens) {
											modifiedImage.CopyPixels(element.Content, screen.Bounds.toRect());
										}
									}
									// We got through the capture without exception
									success = true;
									break;
								} catch (ExternalException ee) {
									LOG.Warn("Problem getting ImageSource at try " + i + " : ", ee);
									exception = ee;
								}
							}
							if (!success) {
								LOG.Error("Still couldn't create ImageSource!");
								throw exception;
							}
						}
					}
				}
			}
			return element;
		}
	}
}
